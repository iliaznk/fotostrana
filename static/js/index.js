'use strict';

// Прозрачность неаквтиной иконки.
var DISABLED_OPACITY = 0.5;

// Класс неактивной иконки.
var ACTIVE_CLASSNAME = 'js-test-disabled';

// Какой атрбут действия action использовать для отсчёта времени.
var DURATION_ATTRIBUTE = 'recovery_time';

// Кнопка отображения картинки с макетом.
var mockupButton = document.getElementById('test-show-mockup');

// Счётчик очков
var scoreCounter = document.getElementsByClassName('test-score-count')[0];
var mockup = document.getElementsByClassName('test-original-image')[0];

// В задании не было указаано, что брать за время N, я решил использовать атрибут recovery_time.
// Для чего нужен атрибут rest_time, я не понял.
var actions = [
    {
        id: 145,
        title: 'Test 1',
        rest_time: 0,
        recovery_time: 60,
        points: 10
    },
    {
        id: 146,
        title: 'Test 2',
        rest_time: 428,
        recovery_time: 10,
        points: 20
    },
    {
        id: 147,
        title: 'Test 3',
        rest_time: 0,
        recovery_time: 3,
        points: 30
    },
    {
        id: 148,
        title: 'Test 4',
        rest_time: 100,
        recovery_time: 5,
        points: 40
    }
];

var current_points = 123;

function hasClass(element, selector) {
    var className = " " + selector + " ";
    return (" " + element.className + " ").replace(/[\t\r\n\f]/g, " ").indexOf(className) > -1;
}

function addClass(element, className) {
    element.className += ' ' + className;
    return element;
}

function removeClass(element, className) {
    element.className = element.className.replace(className, '');
}

// Показать/спрятать макет.
function toggleMockup() {
    var display = mockup.style.display || 'none';

    if (display == 'none') {
        mockup.style.display = 'block';
    } else {
        mockup.style.display = 'none';
    }
}

function updateScore(action){
    scoreCounter.innerText = parseInt(scoreCounter.innerText, 10) + action.points;
}

function cancelScore(action){
    scoreCounter.innerText = parseInt(scoreCounter.innerText, 10) - action.points;
}

function sendRequest(actionID, errorCallback){

    /**
     * ЗАКОММЕНТИРУЙТЕ ЭТУ СТРОЧКУ, ЧТОБЫ ИСПЫТАТЬ С ЛОКАЛЬНЫМ СЕРВЕРОМ
     */
    return;

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
            var resp = {};

            if (xmlhttp.responseText) {
                try {
                    resp = JSON.parse(xmlhttp.responseText);
                } catch (e){
                    errorCallback();
                    return;
                }
            } else {
                errorCallback();
                return;
            }

            if (!resp.status || resp.status !== 'ok') {
                errorCallback();
            }
        }
    };

    xmlhttp.open("POST", "http://localhost:8000", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify({actionID: actionID}));
}

// Принимает DOM-элемент и действие из массива actions.
// Запускает таймер соответствующего элемента и обновляет счётчик очков.
// Отправляет запрос на сервер, в случае ошибки, возвращает счётчик очков в изначальное состояние.
function handleClick(element, action) {
    if (hasClass(element, ACTIVE_CLASSNAME)) {
        return;
    }

    var timeElement = element.childNodes[3];
    var image = element.childNodes[1];
    var timer = action[DURATION_ATTRIBUTE];

    function getMinutesAndSeconds() {
        var minutes = parseInt(timer / 60, 10);
        var seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        return minutes + ':' + seconds;
    }

    function reset() {
        clearInterval(interval);
        removeClass(element, ACTIVE_CLASSNAME);
        image.style.opacity = 1;
        timeElement.innerText = '';
        timeElement.setAttribute('data-text', '');
    }

    var errorCallback = function errorCallback() {
        // В случае ошибки передачи данных
        // отменяем прибавленные очки и ресетим элемент.
        cancelScore(action);
        reset();
        alert('Ошибка связи!');
    };

    addClass(element, ACTIVE_CLASSNAME);
    updateScore(action);
    image.style.opacity = DISABLED_OPACITY;

    var time = getMinutesAndSeconds();

    timeElement.innerText = time;
    timeElement.setAttribute('data-text', time);
    --timer;

    var interval = setInterval(function() {
        var time = getMinutesAndSeconds(timer);

        timeElement.innerText = time;
        timeElement.setAttribute('data-text', time);

        if (--timer < 0) {
            reset();
        }

    }, 1000);

    sendRequest(action.id, errorCallback);
}

// Инициализация приложения с помощью массива всех действий actions.
function initApp(actions) {
    actions.forEach(function(action) {
        // Вдруг будет несколько элементов, выполняющих одно и то же действие.
        var elements = document.getElementsByClassName('action-' + action.id);
        scoreCounter.innerText = '' + current_points;
        for (var i = 0, len = elements.length; i < len; i++) {
            var element = elements[i];
            element.addEventListener('click', function() {
                handleClick(element, action);
            });
        }
    });
}

initApp(actions);

mockupButton.addEventListener('click', toggleMockup);